import React, { useState, useContext, useEffect } from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { connect } from 'react-redux';
import {userAction} from './redux/Action';


function FormsFeilds(props) {



    const initialState = {
        Forms: {
            Id: '',
            UserName: '',
            Email: ''
        },
        tablePage: false
    }
    const [state, setState] = useState(initialState);
    // const { Id, UserName, Email } = state;
    const handleChange = (e) => {
        let value = e.target.value;
        let name = e.target.name;
        console.log(value, "---", name)

        setState(preState => ({
            ...preState,
            Forms: {
                ...preState.Forms,
                [name]: value
            }

        }))
    }

    const handleClick = (e) => {
        e.preventDefault();
        console.log(state.Forms)
        props.userAction({ type: "POST_USER", payload: state.Forms })
        reset()
    }
    const reset = () => {
        let emptyObj = {
            Id: "",
            UserName: "",
            Email: ""
        }
        setState(preState => ({
            ...preState,
            Forms: {
                ...preState.Forms,
                ...emptyObj
            },
        }))
    }

    console.log(state.Forms)
    return (

        <div>
            < div >
                <Form>
                    <FormGroup>
                        <Label for="exampleEmail">ID</Label>
                        <Input type="text"
                            name="Id"
                            value={state.Forms.Id}
                            onChange={handleChange}
                            id="exampleEmail" placeholder="ID" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleEmail">UserName</Label>
                        <Input type="text"
                            name="UserName"
                            value={state.Forms.UserName}
                            onChange={handleChange}
                            id="exampleEmail" placeholder="UserName" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input type="email"
                            name="Email"
                            value={state.Forms.Email}
                            onChange={handleChange}
                            id="examplePassword" placeholder="Email" />
                    </FormGroup>
                </Form>
                <Button color="primary" onClick={handleClick}>Submit</Button>

            </div>

        </div >

    )
}
const mapStateToProps = (state) => {
    return {
        userListOBJ: state.reducerforMyredux
    }
}
const mapDispathToProps = (dispatch) => {
    return {
        userAction: (obj) => dispatch(userAction(obj))
    }
}

export default connect(mapStateToProps,mapDispathToProps)(FormsFeilds)

import React, { useEffect } from 'react'
import { Table } from 'reactstrap';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

function TableFileredux(props) {


    const handleDelete = (item) => {
        console.log('delete--->', item)
    }
    const handleEDIT = (item) => {

        console.log("edit--->true--->", item)
        props.history.push('/')
    }

    useEffect(() => {
        console.log(props.userData)
        return () => {

        }
    }, [props.userData.postData])

    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Edit/Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.userData.postData.map((item, index) => {
                        return (
                            <tr key={index}>
                                <th>{item.Id}</th>
                                <th>{item.UserName}</th>
                                <th>{item.Email}</th>
                                <th>
                                    <Button onClick={() => handleEDIT(item)} color="info">Edit</Button>
                                    <Button onClick={() => handleDelete(item)} color="danger">Delete</Button>
                                </th>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>

    )
}
const mapStateToProps = (state) => {
    return {
        userData: state.reducer
    }
}
export default connect(mapStateToProps)(TableFileredux)


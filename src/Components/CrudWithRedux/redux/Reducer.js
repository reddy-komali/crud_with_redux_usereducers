const initialState = {
    postData: []
}

const reducer = (state = initialState, action) => {
    console.log('reducer-redux-->',action)
    switch (action.type) {

        case "POST_USER":
            return ({
                ...state,
                postData: [
                    ...state.postData, action.payload
                ]
            })


        default:
            return state;
    }
}
export default reducer;
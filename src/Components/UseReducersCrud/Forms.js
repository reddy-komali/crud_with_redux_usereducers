import React, { useState, useContext, useEffect } from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { MainContex } from './Reducers/MainProvider'


function Forms(props) {

    const contextConsumer = useContext(MainContex);
    console.log(contextConsumer)

    const { dispatchFun, stateValue, edit_obj, is_Edit } = contextConsumer;

    const initialState = {
        Forms: {
            Id: '',
            UserName: '',
            Email: ''
        },
        tablePage: false
    }
    const [state, setState] = useState(initialState);
    // const { Id, UserName, Email } = state;
    const handleChange = (e) => {
        let value = e.target.value;
        let name = e.target.name;
        console.log(value, "---", name)

        setState(preState => ({
            ...preState,
            Forms: {
                ...preState.Forms,
                [name]: value
            }

        }))
    }

    const handleClick = (e) => {
        e.preventDefault();
        if (is_Edit === true) {

            dispatchFun({ type: "UPDATE", payload: state.Forms })
            dispatchFun({ type: "EDIT", payload: '', is_Edit:false })

            reset()
        
        } else {

            dispatchFun({ type: "POST_FORM_DATA", payload: state.Forms, is_Edit: false })
            reset()

        }
    }
    const reset = () => {
        let emptyObj = {
            Id: "",
            UserName: "",
            Email: ""
        }
        setState(preState => ({
            ...preState,
            Forms: {
                ...preState.Forms,
                ...emptyObj
            },
        }))
    }
    useEffect(() => {
        console.log(edit_obj)
        setState(pervStete => ({
            ...pervStete,
            Forms: {
                ...pervStete.Forms,
                ...edit_obj
            }

        }))
        return () => {
        }
    }, [Object.keys(edit_obj).length > 0])

    console.log(state.Forms)
    return (

        <div>
            < div >
                <Form>
                    <FormGroup>
                        <Label for="exampleEmail">ID</Label>
                        <Input type="text"
                            name="Id"
                            value={state.Forms.Id}
                            onChange={handleChange}
                            id="exampleEmail" placeholder="ID" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleEmail">UserName</Label>
                        <Input type="text"
                            name="UserName"
                            value={state.Forms.UserName}
                            onChange={handleChange}
                            id="exampleEmail" placeholder="UserName" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input type="email"
                            name="Email"
                            value={state.Forms.Email}
                            onChange={handleChange}
                            id="examplePassword" placeholder="Email" />
                    </FormGroup>
                </Form>
                <Button color="primary" onClick={handleClick}>Submit</Button>

            </div>

        </div >

    )
}

export default Forms

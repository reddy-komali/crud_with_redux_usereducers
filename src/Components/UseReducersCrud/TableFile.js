import React, { useContext } from 'react'
import { MainContex } from './Reducers/MainProvider'
import { Table } from 'reactstrap';

import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

function TableFile(props) {
    const contextConsumer = useContext(MainContex);
    console.log(contextConsumer)

    const { dispatchFun, stateValue, edit_obj, is_Edit } = contextConsumer;

    const handleDelete = (item) => {
        console.log('delete--->', item)
        dispatchFun({ type: "DELETE", payload: item })
    }
    const handleEDIT = (item) => {
        dispatchFun({ type: "EDIT", payload: item, is_Edit: true })
        console.log("edit--->true--->", edit_obj)
        props.history.push('/')
    }

    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Edit/Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {stateValue.map((item, index) => {
                        return (
                            <tr key={index}>
                                <th>{item.Id}</th>
                                <th>{item.UserName}</th>
                                <th>{item.Email}</th>
                                <th>
                                    <Button onClick={() => handleEDIT(item)} color="info">Edit</Button>
                                    <Button onClick={() => handleDelete(item)} color="danger">Delete</Button>
                                </th>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </div>

    )
}

export default TableFile

import React, { useReducer } from 'react'


export const MainContex = React.createContext();

function MainProvider(props) {

    const initialState = {
        UserList: [],
        Edit_Obj: {},
        is_Edit: false,

    }
    const reducer = (state, action) => {
        switch (action.type) {
            case "POST_FORM_DATA":
                return ({
                    ...state,
                    UserList: [
                        ...state.UserList, action.payload
                    ]
                })
            case "EDIT":
                return ({
                    ...state,
                    Edit_Obj: action.payload,
                    is_Edit: action.is_Edit
                })
            case "UPDATE":
                let userlist = state.UserList
                let updatelist = userlist.map((item, index) => {
                        if(item.Id === action.payload.Id ){
                            console.log(item, index)
                             userlist.splice(index,0)
                             return action.payload
                        }
                        return item
                })
                console.log('reducers--->update--->',updatelist)
                return ({
                    ...state,
                    UserList: updatelist
                })

            case "DELETE":

                // const userlist = state.UserList        
                // console.log(userlist)
                return ({
                    ...state,
                    UserList: state.UserList.filter(item => item.Id !== action.payload.Id)
                })


            default:
                break;
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <div>
            <MainContex.Provider value={{
                dispatchFun: dispatch,
                stateValue: state.UserList,
                edit_obj: state.Edit_Obj,
                is_Edit: state.is_Edit
            }}>
                {props.children}
            </MainContex.Provider>
        </div>
    )
}

export default MainProvider;

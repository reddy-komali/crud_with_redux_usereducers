import React, { Fragment } from 'react'
import Forms from './Components/UseReducersCrud/Forms';
import MainProvider from './Components/UseReducersCrud/Reducers/MainProvider';
import { BrowserRouter as Router, Route, Link, Swithc } from 'react-router-dom';
import TableFile from './Components/UseReducersCrud/TableFile';
import FormsFeilds from './Components/CrudWithRedux/FormFeilds';
import TableFileredux from './Components/CrudWithRedux/TableFileredux';
import Store from './Components/CrudWithRedux/redux/Store';

import { Provider } from 'react-redux';

function App() {
  return (
    <div style={{ margin: "40px" }}>

      <Router>
        <MainProvider>

          {/* <Link to="/"><h4>adde Record</h4></Link>
          <Link to="/TableFile"><h4>TableFile</h4></Link> */}
          <Provider store={Store}>
            <Link to="/FormsFeilds"><h4>Forms-redux</h4></Link>
            <Link to="/TableFileredux"><h4>Table-redux</h4></Link>



            {/* <Route exact path="/" component={Forms} />
          <Route exact path="/TableFile" component={TableFile} /> */}
            <Route exact path="/FormsFeilds" component={FormsFeilds} />
            <Route exact path="/TableFileredux" component={TableFileredux} />
          </Provider>
        </MainProvider>
      </Router>

    </div>

  )
}

export default App
